# Detector

Detector is a multi-process project to find moving objects in real-time video.
MotionDetector is the process dedicated to find frames containing motion in a video.
SingleShotDetector is the process dedicated to to find the type of the moving object.
Logger is the process dedicated to provide informations of moving objects on a the standard output.
Communication between process is done by multiprocessing.Queue.

## Usage

Use the `docker-compose` command to build the app container which runs every process.
```console
foo@bar:~/project_root$sudo docker-compose up
```

## Build/Update documentation

Use the command `sphinx-apidoc` command to auto-generate sphinx sources for the project.
```console
foo@bar:~/project_root/docs$sphinx-apidoc -o source/ ../detector
```

Use the command `make html` command to build the documentation.
```console
foo@bar:~/project_root/docs$make html
```

## Run unit tests
Use the file ~/project_root/detector/tests/run_test.py with `python`.
```console
foo@bar:~/project_root$python detector/tests/run_test.py
```