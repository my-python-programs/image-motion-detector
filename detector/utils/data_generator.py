from datetime import datetime
from random import randrange, uniform


def gen_frame_with_motion():
    """
    Generate a video frame with motion data.

    Returns:
        dict: Contains information of a motion detected on a frame.
    """

    random_data = {
        "frame_id": randrange(1, 1000, 1),
        "timestamp": datetime.now(),
        "bounding_box": {
            "x": randrange(1, 1920, 1),
            "y": randrange(1, 1080, 1),
            "width": randrange(1, 1080, 1),
            "height": randrange(1, 1920, 1)
        },
        "speed": uniform(1, 20),
        "direction": {
            "x": uniform(-1, 1),
            "y": uniform(-1, 1)
        }
    }
    return random_data

def gen_video_with_motion():
    """
    Generate a video with motion data.

    Returns:
        list: Contains frames with motion detected.
    """

    image_with_motion_nb = randrange(10, 20, 1)
    image_with_motion = [gen_frame_with_motion() for _ in range(image_with_motion_nb)]
    return image_with_motion

def detection_vector_data_from_motion_vector(motion_vector):
    """
    Build a dict containing informations about class prediction from a MotionVector object and randomized values for prediction.

    Args:
        motion_vector (MotionVector): Represents a frame object where motion was detected.

    Returns:
        dict: Contains information about class prediction of a frame where motion was detected.
    """

    detection_vector_data = {
        "frame_id": motion_vector.frame_id,
        "timestamp": motion_vector.timestamp,
        "bounding_box": motion_vector.bounding_box,
        "class_prediction": {
            "bus": randrange(1, 99, 1),
            "truck": randrange(1, 99, 1),
            "car": randrange(1, 99, 1),
            "bike": randrange(1, 99, 1),
            "scooter": randrange(1, 99, 1),
        }
    }
    return detection_vector_data