from random import randrange
from time import sleep
from multiprocessing import Process
from queue import Empty
from models.detection_vector import DetectionVector
from messages.vector import Vector
from utils.data_generator import detection_vector_data_from_motion_vector
from config import MSG_QUEUE_TIMEOUT


class SingleShotDetector(Process):
    """
    Process used to get frames with motion in a video and predict the type of the moving object.

    Attributes:
        motion_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type MotionVector.
        detection_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type DetectionVector.
        motion_vector_msg_queue (multiprocessing.Manager.Queue): *Queue* shared between process to get frames containing motion from the MotionDetector Process.
        detection_vector_msg_queue (multiprocessing.Manager.Queue): *Queue* shared between process to publish processed frames from motion_vector Queue.
        lock (multiprocessing.Lock): *Lock* to handle processes concurency acces to shared memory
        shutdown_event (multiprocessing.Event): *Event* to handle the shutdown of the process.
    """

    def __init__(self, motion_vectors, detection_vectors, motion_vector_msg_queue, detection_vector_msg_queue, lock, shutdown_event):
        """
        SingleShotDetector constructor.

        Args:
            motion_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type MotionVector.
            detection_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type DetectionVector.
            motion_vector_msg_queue (multiprocessing.Manager.Queue): *Queue* shared between process to get frames containing motion from the MotionDetector Process.
            detection_vector_msg_queue (multiprocessing.Manager.Queue): *Queue* shared between process to publish processed frames from motion_vector Queue.
            lock (multiprocessing.Lock): *Lock* to handle processes concurency acces to shared memory
            shutdown_event (multiprocessing.Event): *Event* to handle the shutdown of the process.
        """

        Process.__init__(self)
        self.motion_vectors = motion_vectors
        self.detection_vectors = detection_vectors
        self.motion_vector_msg_queue = motion_vector_msg_queue
        self.detection_vector_msg_queue = detection_vector_msg_queue
        self.lock = lock
        self.shutdown_event = shutdown_event

    def get_vector_from_queue(self, timeout=MSG_QUEUE_TIMEOUT):
        """
        Get a Vector frame_id in *motion_vector_msg_queue*.

        Args:
            timeout (float): Timeout in second for reading in queue.

        Returns:
            Vector: Object containing the frame_id of a frame with motion detected.
            None: If *motion_vector_msg_queue* is empty.
        """
        try:
            return self.motion_vector_msg_queue.get(block=True, timeout=timeout)
        except Empty:
            return None
    
    def get_motion_vector_from_shared_memory(self, motion_vector_frame_id):
        """
        Get a motion vector from shared memory.

        Args:
            motion_vector_frame_id (int): frame_id of a frame which contains motion.

        Returns:
            MotionVector: Object corresponding to the *motion_vector_frame_id*.
        """

        # Find corresponding obj from the frame_id in shared memory.
        try:
            self.lock.acquire()
            motion_vector = next((obj for obj in self.motion_vectors if obj.frame_id == motion_vector_frame_id), None)
            self.lock.release()
            return motion_vector
        except Exception as e:
            print(str(e))
            return None
    
    def process_motion_vector(self, motion_vector):
        """
        Process a motion vector to predict the type of the moving object.

        Args:
            motion_vector (MotionVector): Object of a frame which contains motion.

        Returns:
            DetectionVector: Contains all the informations to build a *DetectionVector* object.
            None: If something failed.
        """

        # Find corresponding obj from the frame_id in shared memory.
        try:
            # Real prediction on the object type should be done here, instead of detection_vector_data_from_motion_vector(motion_vector) instruction.
            # For this test, we just create detection_vector data set with randomized values.
            detection_vector_data = detection_vector_data_from_motion_vector(motion_vector)
            detection_vector = DetectionVector(**detection_vector_data)
            return detection_vector
        except Exception as e:
            print(str(e))
            return None

    def register_in_shared_memory(self, detection_vector):
        """
        Register the detectionVector object in shared memory for the use of the *Logger* process.

        Args:
            detection_vector (DetectionVector): Object with class prediction of a frame containing motion.
        """

        # Add object in shared memory
        try:
            
            self.lock.acquire()
            self.detection_vectors.append(detection_vector)
            self.lock.release()
        except Exception as e:
            print(str(e))

    def publish_detection_vector(self, detection_vector_frame_id):
        """
        Publish a *DetectionVector* of processed frame in shared memory.

        Args:
            detection_vector_frame_id (dict): Contains the frame_id of the frame containing motion. This frame_id is used to build a Vector object to send a message from *detection_vector_msg_queue* to the *Logger* process.
        """

        msg_args = {"frame_id": detection_vector_frame_id}
        new_msg = Vector(**msg_args)
        # Publish message to logger
        self.detection_vector_msg_queue.put(new_msg)

    def run(self):
        """
        Run the *SingleShotDetector* process.

        """

        while not self.shutdown_event.is_set():
            if (vector_msg := self.get_vector_from_queue()):
                if (motion_vector := self.get_motion_vector_from_shared_memory(vector_msg.frame_id)):
                    if (detection_vector := self.process_motion_vector(motion_vector)):
                        self.register_in_shared_memory(detection_vector)
                        self.publish_detection_vector(detection_vector.frame_id)
