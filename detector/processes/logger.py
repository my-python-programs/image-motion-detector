from random import randrange
from time import sleep
from queue import Empty
from multiprocessing import Process
from models.motion_vector import MotionVector
from models.detection_vector import DetectionVector
from config import MSG_QUEUE_TIMEOUT


class Logger(Process):
    """
    Process used to get informations of the frames with motion detected from the shared memory in order to log them.


    Attributes:
        motion_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type *MotionVector*.
        detection_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type *DetectionVector*.
        detection_vector_msg_queue (multiprocessing.Manager.Queue): *Queue* shared between processes to get processed frames from motion_vectors and detection_vectors.
        lock (multiprocessing.Lock): *Lock* to handle processes concurency acces to shared memory
        shutdown_event (multiprocessing.Event): *Event* to handle the shutdown of the process.
    """

    def __init__(self, motion_vectors, detection_vectors, detection_vector_msg_queue, lock, shutdown_event):
        """
        Logger constructor.

        Args:
            motion_vectors (multiprocessing.Manager.list): *list* shared between processes to get frames containing motion from the *MotionDetector* Process.
            detection_vectors (multiprocessing.Manager.list): *list* shared between processes to get frames containing motion from the *SingleShotDetector* Process.
            detection_vector_msg_queue (multiprocessing.Manager.Queue): *Queue* shared between processes to get id of frames containg motion in order to log them.
            lock (multiprocessing.Lock): *Lock* to handle processes concurency acces to shared memory
            shutdown_event (multiprocessing.Event): *Event* to handle the shutdown of the process.
        """

        Process.__init__(self)
        self.motion_vectors = motion_vectors
        self.detection_vectors = detection_vectors
        self.detection_vector_msg_queue = detection_vector_msg_queue
        self.lock = lock
        self.shutdown_event = shutdown_event
    
    def get_vector_from_queue(self, timeout=MSG_QUEUE_TIMEOUT):
        """
        Get a Vector from queue.

        Args:
            timeout (float): Timeout in second for reading in queue.

        Returns:
            Vector: Contains the frame_id of the object containing a frame with motion.
            None: If queue is empty.
        """
        try:
            return self.detection_vector_msg_queue.get(block=True, timeout=timeout)
        except Empty:
            return None

    def get_vectors_from_shared_memory(self, frame_vector):
        """
        Get a MotionVector and a DetectionVector by *frame_vector.frame_id* from the shared memory.

        Args:
            frame_vector (Vector): Contains the frame_id of the desired objects.

        Returns:
            tuple: Contains the MotionVector and the DetectionVector.
            (None, None): If object cannot be found in shared memory.
        """

        try:
            self.lock.acquire()
            # Get MotionVector from shared memory.
            motion_vector = next((obj for obj in self.motion_vectors if obj.frame_id == frame_vector.frame_id), None)
            # Get DetectionVector from shared memory.
            detection_vector = next((obj for obj in self.detection_vectors if obj.frame_id == frame_vector.frame_id), None)
            self.lock.release()
            return (motion_vector, detection_vector)
        except Exception as e:
            print(str(e))
            print("tamer")
            return (None, None)

    def display_on_std_out(self, motion_vector, detection_vector):
        """
        Display information of the objects on the standard output.

        Args:
            motion_vector (MotionVector): Corresponds to the *MotionVector* object to be displayed.
            detection_vector (DetectionVector): Corresponds to the *DetectionVector* object to be displayed.
        """

        try:
            print("---------------------------------------------------------------------------")
            print("MotionVector:")
            print("* Frame ID =", motion_vector.frame_id)
            print("* TimeStamp =", motion_vector.timestamp)
            print("* Bounding box =", motion_vector.bounding_box)
            print("* Speed =", motion_vector.speed)
            print("* Direction =", motion_vector.direction)
            print("DetectionVector:")
            print("* Frame ID =", detection_vector.frame_id)
            print("* TimeStamp =", detection_vector.timestamp)
            print("* Bounding box =", detection_vector.bounding_box)
            print("* Class prediction =", detection_vector.class_prediction)
        except Exception as e:
            print(str(e))

    def remove_vectors_from_shared_memory(self, motion_vector, detection_vector):
        """
        Remove a consumed *MotionVector* and a consumed *DetectionVector* from the shared memory to free space.

        Args:
            motion_vector (MotionVector): Contains the *MotionVector* object to be removed from shared memory.
            detection_vector (DetectionVector): Contains the *DetectionVector* object to be removed from shared memory.

        Returns:
            True: If objects has been successfully removed.
            False: If objects has been successfully removed.
        """

        try:
            self.lock.acquire()
            self.motion_vectors.remove(motion_vector)
            self.detection_vectors.remove(detection_vector)
            self.lock.release()
            return True
        except Exception as e:
            print(str(e))
            return False

    def run(self):
        """
        Run the *SingleShotDetector* process.

        """

        while not self.shutdown_event.is_set():
            if (frame_vector := self.get_vector_from_queue()):
                motion_vector, detection_vector = self.get_vectors_from_shared_memory(frame_vector)
                if motion_vector and detection_vector:
                    self.display_on_std_out(motion_vector, detection_vector)
                    self.remove_vectors_from_shared_memory(motion_vector, detection_vector)
