from random import randrange
from time import sleep
from multiprocessing import Process
from models.motion_vector import MotionVector
from messages.vector import Vector
from utils.data_generator import gen_video_with_motion


class MotionDetector(Process):
    """
    Process used to get video data and detect motions on it.

    Attributes:
        motion_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type MotionVector.
        motion_vector_queue (multiprocessing.Manager.Queue): *Queue* shared between process to send frames containing motion to the SingleShotDetector Process.
        lock (multiprocessing.Lock): *Lock* to handle processes concurency acces to shared memory
        shutdown_event (multiprocessing.Event): *Event* to handle the shutdown of the process.
    """

    def __init__(self, motion_vectors, motion_vector_msg_queue, lock, shutdown_event):
        """
        MotionDetector constructor.

        Args:
            motion_vectors (multiprocessing.Manager.list): *list* shared between processes containing frames of type MotionVector.
            motion_vector_msg_queue (multiprocessing.Manager.Queue): *Queue* shared between process to send frame_id of frames containing motion to the SingleShotDetector Process.
            lock (multiprocessing.Lock): *Lock* to handle processes concurency acces to shared memory
            shutdown_event (multiprocessing.Event): *Event* to handle the shutdown of the process.
        """

        Process.__init__(self)
        self.motion_vectors = motion_vectors
        self.motion_vector_msg_queue = motion_vector_msg_queue
        self.lock = lock
        self.shutdown_event = shutdown_event

    def get_video_data(self):
        """
        Get video data.

        Returns:
            list: Contains frames from a video.
        """

        # If the program runs in a server, then real video data should be fetched from an API.
        # If the program is embbeded with a sensor, real-time data should be fetched by reading permanently the port on which
        # the sensor sends data.
        return gen_video_with_motion()

    def detect_motion(self, video_data):
        """
        Detects frames with motion in *video_data*.

        Args:
            video_data (list): Frames of the video being processed.

        Returns:
            list: Contains frames with motion from *video_data*.
        """

        # Fake the motion detection. Real motion detection should be done here.
        # For this tecnical test, we will consider that every generated frames in video_data contains motion.
        return video_data

    def register_and_publish_frames_with_motion(self, frames_with_motion):
        """
        Register frames containing motion in shared memory. Publish a *Vector* containing their frame_id in a message *Queue*

        Args:
            frames_with_motion (list): Frames of video which contains motion.
        """
        try:
            self.lock.acquire()
            for frame in frames_with_motion:
                motion_vector = MotionVector(**frame)
                self.motion_vectors.append(motion_vector)
                msg_args = {"frame_id": frame['frame_id']}
                new_msg = Vector(**msg_args)
                self.motion_vector_msg_queue.put(new_msg)
            self.lock.release()
        except Exception as e:
            print(str(e))

    def run(self):
        """
        Run the *MotionDetector* process.

        """

        while not self.shutdown_event.is_set():
            frames_with_motion = self.detect_motion(self.get_video_data())
            self.register_and_publish_frames_with_motion(frames_with_motion)
            # In order to keep a standard output readable, we will generate fake data and detect fake motion on them only every 10s.
            sleep(10)
