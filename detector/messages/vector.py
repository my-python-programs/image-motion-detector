from pydantic import BaseModel


class Vector(BaseModel):
    """
    Model used to send messages between processes.

    Attributes:
        frame_id (int): The identifier of the frame where motion was detected.
    """
    
    frame_id: int