from datetime import datetime
from typing import Dict
from pydantic import BaseModel


class DetectionVector(BaseModel):
    """
    Model used to store class prediction on a frame where motion was detected.

    Attributes:
        frame_id (int): The identifier of the frame where motion was detected.
        timestamp (datetime): Timestamp of the frame creation
        bounding_box (dict): Dict with *x*, *y*, *width* and *height* int values to get precise location of the detected motion on the frame.
        class_prediction (dict): Dict containing types of object (eg: car, bike, etc.) and the associated prediction value.
    """
    
    frame_id: int
    timestamp: datetime
    bounding_box: Dict
    class_prediction: Dict