from datetime import datetime
from typing import Dict
from pydantic import BaseModel


class MotionVector(BaseModel):
    """
    Model used to store informations of a frame where motion was detected.

    Attributes:
        frame_id (int): The identifier of the frame where motion was detected.
        timestamp (datetime): Timestamp of the frame creation
        bounding_box (dict): Dict with *x*, *y*, *width* and *height* int values to get precise location of the detected motion on the frame.
        speed (float): Speed of the detected moving object.
        direction (dict): Dict with *x* and *y* float values to get precise direction of the detected motion on the frame.
    """

    frame_id: int
    timestamp: datetime
    bounding_box: Dict
    speed: float
    direction: Dict