import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from multiprocessing import Queue, Manager, Lock, Event
from processes.motion_detector import MotionDetector
from processes.single_shot_detector import SingleShotDetector
from processes.logger import Logger
import signal


def start_pool():
    """
    Runs every processes with multiprocessing.
    """

    with Manager() as manager:
        lock = Lock()
        shutdown_event = Event()
        motion_vectors = manager.list()
        detection_vectors = manager.list()
        motion_vector_msg_queue = manager.Queue()
        detection_vector_msg_queue = manager.Queue()

        # Init processes
        motion_detector_process = MotionDetector(
            motion_vectors,
            motion_vector_msg_queue,
            lock,
            shutdown_event
        )
        single_shot_detector_process = SingleShotDetector(
            motion_vectors, 
            detection_vectors,
            motion_vector_msg_queue,
            detection_vector_msg_queue,
            lock,
            shutdown_event
        )
        logger_process = Logger(
            motion_vectors, 
            detection_vectors,
            detection_vector_msg_queue,
            lock,
            shutdown_event
        )

        # Start processes
        motion_detector_process.start()
        single_shot_detector_process.start()
        logger_process.start()

        # Kill every processes on signal received.
        def stop_process(*args):
            shutdown_event.set()
            motion_detector_process.terminate()
            single_shot_detector_process.terminate()
            logger_process.terminate()

        signal.signal(signal.SIGINT, stop_process) # Interupt from keyboard (Ctrl-c)

        # Wait until processes are running
        motion_detector_process.join()
        single_shot_detector_process.join()
        logger_process.join()

