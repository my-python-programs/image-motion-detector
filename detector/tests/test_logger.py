import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from datetime import datetime
from unittest import TestCase
from multiprocessing import Queue, Manager, Lock, Event

from processes.logger import Logger
from models.motion_vector import MotionVector
from models.detection_vector import DetectionVector
from messages.vector import Vector


class TestLogger(TestCase):

    def setUp(self):
        manager = Manager()
        self.lock = Lock()
        self.shutdown_event = Event()
        self.motion_vectors = manager.list()
        self.detection_vectors = manager.list()
        self.detection_vector_msg_queue = manager.Queue()
        self.logger_process = Logger(
            self.motion_vectors,
            self.detection_vectors,
            self.detection_vector_msg_queue,
            self.lock,
            self.shutdown_event
        )

    def test_get_vector_from_queue(self):
        frame_vector = Vector(frame_id=1)
        self.detection_vector_msg_queue.put(frame_vector)
        self.assertEqual(frame_vector, self.logger_process.get_vector_from_queue(timeout=0.1))

    def test_get_vector_from_empty_queue(self):
        self.assertEqual(None, self.logger_process.get_vector_from_queue(timeout=0.1))

    def test_get_vectors_from_shared_memory(self):
        motion_vector = MotionVector(
            frame_id=1,
            timestamp=datetime.now(),
            bounding_box={"x":10,"y":10,"width":10, "height": 10},
            speed=1.0001,
            direction={"x": 1.0001, "y": 1.0001}
        )
        detection_vector = DetectionVector(
            frame_id=1,
            timestamp=datetime.now(),
            bounding_box={"x":10,"y":10,"width":10, "height": 10},
            class_prediction={"bus": 1, "truck": 1, "car": 95, "bike": 1, "scooter": 1}
        )
        frame_vector = Vector(frame_id=1)
        self.motion_vectors.append(motion_vector)
        self.detection_vectors.append(detection_vector)
        self.assertEqual((motion_vector, detection_vector), self.logger_process.get_vectors_from_shared_memory(frame_vector))
    
    def test_get_vectors_from_shared_memory_fail(self):
        # Test with a frame_id of a non existing object in shared memory.
        frame_vector = Vector(frame_id=1)
        self.assertEqual((None, None), self.logger_process.get_vectors_from_shared_memory(frame_vector))

    def test_remove_vectors_from_shared_memory(self):
        motion_vector = MotionVector(
            frame_id=1,
            timestamp=datetime.now(),
            bounding_box={"x":10,"y":10,"width":10, "height": 10},
            speed=1.0001,
            direction={"x": 1.0001, "y": 1.0001}
        )
        detection_vector = DetectionVector(
            frame_id=1,
            timestamp=datetime.now(),
            bounding_box={"x":10,"y":10,"width":10, "height": 10},
            class_prediction={"bus": 1, "truck": 1, "car": 95, "bike": 1, "scooter": 1}
        )
        self.motion_vectors.append(motion_vector)
        self.detection_vectors.append(detection_vector)
        # Check if object has been added in shared memory
        self.assertEqual(motion_vector, self.motion_vectors[0])
        self.assertEqual(detection_vector, self.detection_vectors[0])
        # Check if objects has been removed from shared memory
        self.assertEqual(True, self.logger_process.remove_vectors_from_shared_memory(motion_vector, detection_vector))
        self.assertEqual(0, len(self.motion_vectors))
        self.assertEqual(0, len(self.detection_vectors))