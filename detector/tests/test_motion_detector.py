import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from datetime import datetime
from unittest import TestCase
from processes.motion_detector import MotionDetector
from messages.vector import Vector
from multiprocessing import Queue, Manager, Lock, Event


class TestMotionDetector(TestCase):

    def setUp(self):
        manager = Manager()
        self.lock = Lock()
        self.shutdown_event = Event()
        self.motion_vectors = manager.list()
        self.motion_vector_msg_queue = manager.Queue()
        self.motion_detector_process = MotionDetector(
            self.motion_vectors,
            self.motion_vector_msg_queue,
            self.lock,
            self.shutdown_event
        )

    # This test will change when MotionDetector.get_video_data() will return actual real-time data and not only data with motion.
    # During this technical test, we consider that every video data contains motion.
    def test_get_video_data(self):
        video = self.motion_detector_process.get_video_data()
        self.assertEqual(list, type(video))
        self.assertNotEqual(0, len(video))
        for frame in video:
            self.assertEqual(True, "frame_id" in frame)
            self.assertEqual(int, type(frame["frame_id"]))
            self.assertEqual(True, "timestamp" in frame)
            self.assertEqual(datetime, type(frame["timestamp"]))
            self.assertEqual(True, "bounding_box" in frame)
            self.assertEqual(dict, type(frame["bounding_box"]))
            self.assertEqual(True, "x" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["x"]))
            self.assertEqual(True, "y" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["y"]))
            self.assertEqual(True, "width" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["width"]))
            self.assertEqual(True, "height" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["height"]))
            self.assertEqual(True, "speed" in frame)
            self.assertEqual(float, type(frame["speed"]))
            self.assertEqual(True, "direction" in frame)
            self.assertEqual(dict, type(frame["direction"]))
            self.assertEqual(True, "x" in frame["direction"])
            self.assertEqual(float, type(frame["direction"]["x"]))
            self.assertEqual(True, "y" in frame["direction"])
            self.assertEqual(float, type(frame["direction"]["y"]))

    def test_detect_motion(self):
        video = self.motion_detector_process.get_video_data()
        self.assertEqual(self.motion_detector_process.detect_motion(video), video)
        self.assertEqual(list, type(video))
        self.assertNotEqual(0, len(video))
        for frame in video:
            self.assertEqual(True, "frame_id" in frame)
            self.assertEqual(int, type(frame["frame_id"]))
            self.assertEqual(True, "timestamp" in frame)
            self.assertEqual(datetime, type(frame["timestamp"]))
            self.assertEqual(True, "bounding_box" in frame)
            self.assertEqual(dict, type(frame["bounding_box"]))
            self.assertEqual(True, "x" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["x"]))
            self.assertEqual(True, "y" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["y"]))
            self.assertEqual(True, "width" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["width"]))
            self.assertEqual(True, "height" in frame["bounding_box"])
            self.assertEqual(int, type(frame["bounding_box"]["height"]))
            self.assertEqual(True, "speed" in frame)
            self.assertEqual(float, type(frame["speed"]))
            self.assertEqual(True, "direction" in frame)
            self.assertEqual(dict, type(frame["direction"]))
            self.assertEqual(True, "x" in frame["direction"])
            self.assertEqual(float, type(frame["direction"]["x"]))
            self.assertEqual(True, "y" in frame["direction"])
            self.assertEqual(float, type(frame["direction"]["y"]))

    def test_register_and_publish_frames_with_motion(self):
        video_data = self.motion_detector_process.get_video_data()
        # Since we test MotionDetector.get_video_data() above, we consider that this method is validated so we
        # will test MotionDetecto.publish_frames_with_motion() on the first frame only.
        self.motion_detector_process.register_and_publish_frames_with_motion(video_data[:1])
        queue_msg= self.motion_detector_process.motion_vector_msg_queue.get()
        mock_vector = Vector(frame_id=video_data[0]["frame_id"])
        self.assertEqual(mock_vector, queue_msg)
        # test motionVector in shared memory
        frame = self.motion_vectors[0]
        self.assertEqual(int, type(frame.frame_id))
        self.assertEqual(datetime, type(frame.timestamp))
        self.assertEqual(dict, type(frame.bounding_box))
        self.assertEqual(int, type(frame.bounding_box["x"]))
        self.assertEqual(int, type(frame.bounding_box["y"]))
        self.assertEqual(int, type(frame.bounding_box["width"]))
        self.assertEqual(int, type(frame.bounding_box["height"]))
        self.assertEqual(float, type(frame.speed))
        self.assertEqual(dict, type(frame.direction))
        self.assertEqual(float, type(frame.direction["x"]))
        self.assertEqual(float, type(frame.direction["y"]))