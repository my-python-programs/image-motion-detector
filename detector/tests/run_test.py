import unittest
from test_motion_detector import TestMotionDetector
from test_single_shot_detector import TestSingleShotDetector
from test_logger import TestLogger

if __name__ == '__main__':
    unittest.main()