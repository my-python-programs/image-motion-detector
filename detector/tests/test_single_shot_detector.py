import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from datetime import datetime
from unittest import TestCase
from processes.single_shot_detector import SingleShotDetector
from messages.vector import Vector
from models.motion_vector import MotionVector
from models.detection_vector import DetectionVector
from multiprocessing import Queue, Manager, Lock, Event


class TestSingleShotDetector(TestCase):

    def setUp(self):
        manager = Manager()
        self.lock = Lock()
        self.shutdown_event = Event()
        self.motion_vectors = manager.list()
        self.detection_vectors = manager.list()
        self.motion_vector_msg_queue = manager.Queue()
        self.detection_vector_msg_queue = manager.Queue()
        self.single_shot_detector_process = SingleShotDetector(
            self.motion_vectors,
            self.detection_vectors,
            self.motion_vector_msg_queue,
            self.detection_vector_msg_queue,
            self.lock,
            self.shutdown_event
        )

    def test_get_vector_from_queue(self):
        frame_vector = Vector(frame_id=1)
        self.motion_vector_msg_queue.put(frame_vector)
        self.assertEqual(frame_vector, self.single_shot_detector_process.get_vector_from_queue(timeout=0.1))

    def test_get_vector_from_empty_queue(self):
        self.assertEqual(None, self.single_shot_detector_process.get_vector_from_queue(timeout=0.1))

    def test_get_motion_vector_from_shared_memory(self):
        motion_vector = MotionVector(
            frame_id=1,
            timestamp=datetime.now(),
            bounding_box={"x":10,"y":10,"width":10, "height": 10},
            speed=1.0001,
            direction={"x": 1.0001, "y": 1.0001}
        )
        self.motion_vectors.append(motion_vector)
        self.assertEqual(motion_vector, self.single_shot_detector_process.get_motion_vector_from_shared_memory(motion_vector.frame_id))
    
    def test_get_motion_vector_from_shared_memory_fail(self):
        # test with a frame_id of a non existing object in shared memory.
        self.assertEqual(None, self.single_shot_detector_process.get_motion_vector_from_shared_memory(1))

    def test_process_motion_vector(self):
        motion_vector = MotionVector(
            frame_id="1",
            timestamp=datetime.now(),
            bounding_box={"x":10,"y":10,"width":10, "height": 10},
            speed=1.0001,
            direction={"x": 1.0001, "y": 1.0001}
        )
        detection_vector = self.single_shot_detector_process.process_motion_vector(motion_vector)
        self.assertEqual(DetectionVector, type(detection_vector))
        self.assertEqual(int, type(detection_vector.frame_id))
        self.assertEqual(datetime, type(detection_vector.timestamp))
        self.assertEqual(dict, type(detection_vector.bounding_box))
        self.assertEqual(True, "x" in detection_vector.bounding_box)
        self.assertEqual(int, type(detection_vector.bounding_box["x"]))
        self.assertEqual(True, "y" in detection_vector.bounding_box)
        self.assertEqual(int, type(detection_vector.bounding_box["y"]))
        self.assertEqual(True, "width" in detection_vector.bounding_box)
        self.assertEqual(int, type(detection_vector.bounding_box["width"]))
        self.assertEqual(True, "height" in detection_vector.bounding_box)
        self.assertEqual(int, type(detection_vector.bounding_box["height"]))
        self.assertEqual(dict, type(detection_vector.class_prediction))
        self.assertEqual(True, "bus" in detection_vector.class_prediction)
        self.assertEqual(int, type(detection_vector.class_prediction["bus"]))
        self.assertEqual(True, "truck" in detection_vector.class_prediction)
        self.assertEqual(int, type(detection_vector.class_prediction["truck"]))
        self.assertEqual(True, "car" in detection_vector.class_prediction)
        self.assertEqual(int, type(detection_vector.class_prediction["car"]))
        self.assertEqual(True, "bike" in detection_vector.class_prediction)
        self.assertEqual(int, type(detection_vector.class_prediction["bike"]))
        self.assertEqual(True, "scooter" in detection_vector.class_prediction)
        self.assertEqual(int, type(detection_vector.class_prediction["scooter"]))

    def test_register_in_shared_memory(self):
        detection_vector = DetectionVector(
            frame_id="1",
            timestamp=datetime.now(),
            bounding_box={"x":10,"y":10,"width":10, "height": 10},
            class_prediction={"bus": 1, "truck": 1, "car": 95, "bike": 1, "scooter": 1}
        )
        self.single_shot_detector_process.register_in_shared_memory(detection_vector)
        self.assertEqual(detection_vector, self.single_shot_detector_process.detection_vectors[0])
    
    def test_publish_detection_vector(self):
        frame_vector = Vector(frame_id=1)
        self.single_shot_detector_process.publish_detection_vector(frame_vector.frame_id)
        self.assertEqual(frame_vector, self.single_shot_detector_process.detection_vector_msg_queue.get())
