processes package
=================

Submodules
----------

processes.logger module
-----------------------

.. automodule:: processes.logger
   :members:
   :undoc-members:
   :show-inheritance:

processes.motion\_detector module
---------------------------------

.. automodule:: processes.motion_detector
   :members:
   :undoc-members:
   :show-inheritance:

processes.single\_shot\_detector module
---------------------------------------

.. automodule:: processes.single_shot_detector
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: processes
   :members:
   :undoc-members:
   :show-inheritance:
