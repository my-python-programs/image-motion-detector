models package
==============

Submodules
----------

models.detection\_vector module
-------------------------------

.. automodule:: models.detection_vector
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex:

models.motion\_vector module
----------------------------

.. automodule:: models.motion_vector
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex:

Module contents
---------------

.. automodule:: models
   :members:
   :undoc-members:
   :show-inheritance:
   :noindex:
